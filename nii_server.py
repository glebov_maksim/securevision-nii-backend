# -*- coding: utf-8-*-

import base64
import pynii
import random

from PIL import Image as PILImage
from io import BytesIO
from flask import Flask, jsonify, request
from flask_redis import FlaskRedis
from webargs import fields
from webargs.flaskparser import use_args, parser


app = Flask(__name__)
redis_store = FlaskRedis(app)
pynii.initialize()


@app.errorhandler(422)
def handle_bad_request(err):
    data = getattr(err, 'data')
    if data:
        err_message = data['messages']
    else:
        err_message = 'Invalid request'
    return jsonify({
        'message': err_message,
    }), 422


# -----------------------------------------------------------------------
create_template_args = {
    'image': fields.Str(required=True),  # base64
    'id': fields.Int(required=False),
}


@app.route('/templates', methods=['PUT'])
@use_args(create_template_args)
def create_template(args):
    """
    Выделяет шаблон из фотографии. Опционально сохраняет его 
    (либо просто отдаёт информацию о найденном лице)
    """
    image = PILImage.open(BytesIO(base64.standard_b64decode(args['image'])))

    pynii.initialize()
    pynii_image = pynii.Image(image)

    faces = pynii_image.get_faces(min_face_size=20)
    if len(faces) == 0:
        return ''

    inspect_only = not bool(args['id'])
    id = args['id'] or random.SystemRandom().randint(2**31, 2**63)

    extractor = pynii.Extractor()
    template = extractor.extract_template(pynii_image,
                                          faces[0],
                                          id)

    if not inspect_only:
        with open('face_templates/%s.nii' % id, 'w') as f:
            f.write(template.to_bytes())

    return jsonify({'id': id,
                    'quality': template.get_quality(),
                    'face_location': {
                        'x_coord': faces[0].nX,
                        'y_coord': faces[0].nY,
                        'height': faces[0].nW,
                        'width': faces[0].nH}
                    })


# -----------------------------------------------------------------------
@app.route('/templates', methods=['DELETE'])
def delete_templates():
    """
    Удаляет шаблонЫ
    """


# -----------------------------------------------------------------------
search_in_templates_args = {
    'id': fields.Str(required=True),
    'candidates': fields.List(fields.Int(),
                              required=True)
}


@app.route('/templates/search', methods=['GET'])
@use_args(search_in_templates_args)
def search_in_templates(args):
    """
    Производит поиск среди списка шаблонов
    """
    storage = pynii.TemplateStorage()
    with open('face_templates/%s.nii' % args['id'], 'r') as f:
        template_for_checking = pynii.Template.from_bytes(f.read())

    for candidate in args['candidates']:
        with open('face_templates/%s.nii' % candidate, 'r') as f:
            template = pynii.Template.from_bytes(f.read())
        storage.add(template)

    identifier = pynii.Identifier()
    matches = identifier.identify(storage,
                                  template_for_checking)
    if not matches:
        return jsonify({'best_matches': None})

    return jsonify({'best_match': {
        'id': matches[0].UniqueID,
        'score': matches[0].score
    }})


# -----------------------------------------------------------------------
create_list_args = {
    'list_id': fields.Str(required=True),
    'templates': fields.List(fields.Int(),
                             required=True,
                             validate=lambda p: len(p) > 0)
}


@app.route('/lists', methods=['POST'])
@use_args(create_list_args)
def create_list(args):
    """
    Создаёт список шаблонов
    """
    redis_store.lpush(args['list_id'],
                      *args['templates'])
    return jsonify()


# -----------------------------------------------------------------------
@app.route('/lists/<string:list_id>', methods=['GET'])
def get_list_templates(list_id):
    """
    Отдаёт перечень шаблонов списка 
    """
    templates = redis_store.lrange(list_id, 0, -1)
    return jsonify({'list_id': list_id,
                    'templates': templates})


# -----------------------------------------------------------------------
change_list_templates_args = {
    'templates': fields.List(fields.Int(required=True)),
    'action': fields.Str(validate=lambda p: p == 'put' or 'delete',
                         required=True)
}


@app.route('/lists/<string:list_id>', methods=['PATCH'])
def change_list_templates(list_id):
    """
    Добавляет/удаляет переданные шаблоны в список
    """
    data = parser.parse(change_list_templates_args, request)
    if data['action'] == 'put':
        redis_store.lpush(list_id,
                          *data['templates'])

    elif data['action'] == 'delete':
        for template in data['templates']:
            redis_store.lrem(list_id,
                             0,
                             template)
    return jsonify()


# -----------------------------------------------------------------------
@app.route('/lists/<string:list_id>', methods=['DELETE'])
def delete_list(list_id):
    """
    Удаляет список
    """


# -----------------------------------------------------------------------
search_in_list_args = {
    'id': fields.Int(required=True),
}


@app.route('/lists/<string:list_id>/search', methods=['GET'])
def search_in_list(list_id):
    """
    Производит поиск в списке
    """
    data = parser.parse(search_in_list_args, request)
    storage = pynii.TemplateStorage()
    with open('face_templates/%s.nii' % data['id'], 'r') as f:
        template_for_checking = pynii.Template.from_bytes(f.read())

    candidates = redis_store.lrange(list_id, 0, -1)
    for candidate in candidates:
        with open('face_templates/%s.nii' % candidate, 'r') as f:
            template = pynii.Template.from_bytes(f.read())
        storage.add(template)

    identifier = pynii.Identifier()
    matches = identifier.identify(storage,
                                  template_for_checking)
    if not matches:
        return jsonify({'best_matches': None})

    return jsonify({'best_match': {
        'id': matches[0].UniqueID,
        'score': matches[0].score
    }})


# -----------------------------------------------------------------------
if __name__ == '__main__':
    app.run()
